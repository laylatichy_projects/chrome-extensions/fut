module.exports = {
    pages:         {},
    pluginOptions: {
        browserExtension: {
            artifactFilename: ({ name, version, mode }) => `${name}-${version}-${mode}.zip`,
            componentOptions: {
                // background:     {
                //     entry: 'src/background.js',
                // },
                contentScripts: {
                    entries: {
                        content:    [
                            'src/content.js',
                            'src/hooks/futbin/index.js',
                            'src/store/index.js',
                        ],
                        background: [
                            'src/background.js',
                            'src/hooks/futbin/index.js',
                            // 'src/store/index.js',
                        ],
                    },
                },
            },
        },
    },
    css:           {
        extract: {
            ignoreOrder: true,
            filename:    'css/content.css',
        },
    },
};
