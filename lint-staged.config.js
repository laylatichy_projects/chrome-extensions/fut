module.exports = {
    '**/*.vue':  [
        './node_modules/.bin/eslint .',
    ],
    '**/*.js':   [
        './node_modules/.bin/eslint .',
    ],
    '**/*.less': [
        './node_modules/.bin/stylelint',
    ],
    '**/*.scss': [
        './node_modules/.bin/stylelint',
    ],
};
