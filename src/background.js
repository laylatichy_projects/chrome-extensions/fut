import { getInfo, getPrice, getResourceId } from '@/hooks/futbin';

browser.browserAction.onClicked.addListener((tab) => {
    console.log(tab);
    console.log('Hello from the background');
});

chrome.runtime.onMessage.addListener(
    (request, sender, sendResponse) => {
        if (request.type === 'getPlayerInfo') {
            getInfo(request.player).then((res) => sendResponse(res));

            return true;
        }
        if (request.type === 'getPlayerResourceId') {
            getResourceId(request.id).then((res) => sendResponse(res));

            return true;
        }
        if (request.type === 'getPlayerPrice') {
            getPrice(request.id).then((res) => sendResponse(res));

            return true;
        }
    },
);
