export const getInfo = (player) => new Promise(async (resolve) => {
    try {
        const url = encodeURI(`${process.env.VUE_APP_FUTBIN_INFO_URL}${player.name}`);

        const response = await fetch(url);

        const data = await response.json();

        let playerData = data.find((p) =>
            (
                p.name === player.name || p.url_name === player.name || p.name.includes(player.name) || p.url_name.includes(player.name)) && p.rating === player.rating,
        );

        console.log({ playerData });

        if (!playerData) {
            const name = player.name
                .normalize('NFKD')
                .replace(/-/g, ' ')
                .split(' ')
                .map((n) => n.replace(/[^\w]/g, ''))
                .join(' ');

            playerData = data.find((p) =>
                (p.name === name || p.url_name === name) && p.rating === player.rating,
            );
        }

        resolve(playerData);
    } catch (e) {
        console.log('catch', e);
        resolve(false);
    }
});

export const getResourceId = (id) => new Promise(async (resolve) => {
    try {
        const response = await fetch(`${process.env.VUE_APP_FUTBIN_RESOURCE_URL}${id}`);

        const text = await response.text();

        const parser = new DOMParser();
        const dom = parser.parseFromString(text, 'text/html');

        const resourceId = dom.getElementById('page-info')?.dataset?.playerResource;

        resolve(resourceId);
    } catch (e) {
        console.log('catch', e);
        resolve(false);
    }
});

export const getPrice = (id) => new Promise(async (resolve) => {
    try {
        if (!id) {
            resolve(false);
        }
        const response = await fetch(`${process.env.VUE_APP_FUTBIN_PRICE_URL}${id}`);

        const data = await response.json();

        resolve(data);
    } catch (e) {
        console.log('catch', e);
        resolve(false);
    }
});
