import store      from '@/store';

(async () => {
    await store.dispatch('webapp/waitForWebApp');

    await store.dispatch('logger/info', {
        message: 'web app loaded',
    }, {
        root: true,
    });

    await store.dispatch('webapp/watchMenuChanges');

    store.watch((state) => state.webapp.menu, async (current, previous) => {
        await store.dispatch(`webapp/${previous}/destroy`);
        await store.dispatch(`webapp/${current}/init`);
    });
})();
