const state = {};

const mutations = {};

const actions = {
    displayPlayerInfo({ dispatch }, payload) {
        return new Promise(async (resolve) => {
            document.getElementById('futbin-fetch-prices')?.remove();
            await dispatch('helpers/waitForElement', {
                target: '.ut-item-loaded',
            }, {
                root: true,
            });

            setTimeout(async () => {
                let buttons =
                        document.querySelectorAll('.DetailPanel > .ut-button-group')[0] ||
                        document.querySelectorAll('.DetailPanel > .tradeOptions > .ut-button-group')[0];

                let div = await dispatch('createElement', {
                    type: 'div',
                    id:   'futbin-fetch-prices',
                });

                let button = await dispatch('createElement', {
                    type:  'button',
                    id:    'view-on-futbin',
                    class: 'more',
                });

                if (payload.player?.id) {
                    await dispatch('logger/successAlt', {
                        message: `displaying player data: ${payload.player.name} [${payload.player.id}]`,
                    }, {
                        root: true,
                    });

                    button.addEventListener('click', () => {
                        window.open(payload.player.futbin, '_blank').focus();
                    });

                    let buttonText = await dispatch('createElement', {
                        type:  'span',
                        class: 'btn-text',
                        text:  'View on futbin',
                    });

                    let buttonSubText = await dispatch('createElement', {
                        type:  'span',
                        class: 'btn-subtext',
                    });

                    button.append(buttonText);
                    button.append(buttonSubText);

                    div.append(button);

                    for (const [key, value] of Object.entries(payload.player.prices)) {
                        let button = await dispatch('createElement', {
                            type: 'button',
                        });

                        let buttonText = await dispatch('createElement', {
                            type:  'span',
                            class: 'btn-text',
                            text:  key,
                        });

                        let buttonSubText = await dispatch('createElement', {
                            type:  'span',
                            class: 'btn-subtext',
                            text:  value,
                        });

                        button.append(buttonText);
                        button.append(buttonSubText);

                        div.append(button);
                    }
                } else {
                    let buttonText = await dispatch('createElement', {
                        type:  'span',
                        class: 'btn-text',
                        text:  'unable to fetch player',
                    });

                    let buttonSubText = await dispatch('createElement', {
                        type:  'span',
                        class: 'btn-subtext',
                    });

                    button.append(buttonText);
                    button.append(buttonSubText);

                    div.append(button);
                }

                buttons.append(div);

                await dispatch('listItemButton', {
                    player: payload.player,
                });

                return resolve(true);
            }, 1e2);
        });
    },
    listItemButton({ dispatch }, payload) {
        return new Promise(async (resolve) => {
            document.getElementById('list-item')?.remove();
            document.getElementById('list-item-83')?.remove();

            let listContainer = document
                .querySelector('.ut-quick-list-panel-view > .ut-button-group');

            if (listContainer && payload.player?.prices) {
                let listItemButton = await dispatch('createElement', {
                    type:  'button',
                    id:    'list-item',
                    class: 'more',
                });

                listItemButton.addEventListener('click', async () => {
                    let inputs = document
                        .querySelectorAll('.numericInput.filled');
                    let pricingRanges = document
                        .querySelectorAll('.currency-coins.bandingLabel');

                    let price = payload.player.prices.LCPrice;

                    if (price === 0) {
                        price = pricingRanges[1].textContent.replace(/\D+/g, '');
                    }

                    if (price <= 750) {
                        inputs[0].value = price - 50;
                        inputs[1].value = price;
                    } else if (price < 1000) {
                        inputs[0].value = price - 100;
                        inputs[1].value = price - 50;
                    } else if (price <= 10000) {
                        inputs[0].value = price - 200;
                        inputs[1].value = price - 100;
                    } else if (price <= 100000) {
                        inputs[0].value = price - 500;
                        inputs[1].value = price - 250;
                    } else if (price <= 10000000) {
                        inputs[0].value = price - 2000;
                        inputs[1].value = price - 1000;
                    }

                    document
                        .querySelector('.panelActions')
                        .classList
                        .add('open');

                    const submit = document
                        .querySelector('.panelActions')
                        .querySelector('.call-to-action');

                    await dispatch('helpers/triggerMouseEvent', {
                        type:   'mouseover',
                        target: submit,
                    }, {
                        root: true,
                    });

                    await dispatch('helpers/triggerMouseEvent', {
                        type:   'mousedown',
                        target: submit,
                    }, {
                        root: true,
                    });

                    await dispatch('helpers/triggerMouseEvent', {
                        type:   'mouseup',
                        target: submit,
                    }, {
                        root: true,
                    });

                    await dispatch('toast/success', {
                        message:  `${payload.player.name} listed for: ${inputs[0].value}-${inputs[1].value}`,
                        duration: 3000,
                    }, {
                        root: true,
                    });

                    await dispatch('logger/info', {
                        message: `${payload.player.name} listed for: ${inputs[0].value}-${inputs[1].value}`,
                    }, {
                        root: true,
                    });
                });

                let buttonText = await dispatch('createElement', {
                    type:  'span',
                    class: 'btn-text',
                    text:  'List for best value',
                });

                let buttonSubText = await dispatch('createElement', {
                    type:  'span',
                    class: 'btn-subtext',
                });

                listItemButton.append(buttonText);
                listItemButton.append(buttonSubText);

                listContainer.append(listItemButton);
            }

            if (listContainer) {
                let listItemButton = await dispatch('createElement', {
                    type:  'button',
                    id:    'list-item-83',
                    class: 'more',
                });

                listItemButton.addEventListener('click', async () => {
                    let inputs = document
                        .querySelectorAll('.numericInput.filled');

                    inputs[0].value = 950;
                    inputs[1].value = 1000;

                    document
                        .querySelector('.panelActions')
                        .classList
                        .add('open');

                    const submit = document
                        .querySelector('.panelActions')
                        .querySelector('.call-to-action');

                    await dispatch('helpers/triggerMouseEvent', {
                        type:   'mouseover',
                        target: submit,
                    }, {
                        root: true,
                    });

                    await dispatch('helpers/triggerMouseEvent', {
                        type:   'mousedown',
                        target: submit,
                    }, {
                        root: true,
                    });

                    await dispatch('helpers/triggerMouseEvent', {
                        type:   'mouseup',
                        target: submit,
                    }, {
                        root: true,
                    });

                    await dispatch('toast/success', {
                        message:  `${payload.player.name} listed for: ${inputs[0].value}-${inputs[1].value}`,
                        duration: 3000,
                    }, {
                        root: true,
                    });

                    await dispatch('logger/info', {
                        message: `${payload.player.name} listed for: ${inputs[0].value}-${inputs[1].value}`,
                    }, {
                        root: true,
                    });
                });

                let buttonText = await dispatch('createElement', {
                    type:  'span',
                    class: 'btn-text',
                    text:  'List 83 fodder',
                });

                let buttonSubText = await dispatch('createElement', {
                    type:  'span',
                    class: 'btn-subtext',
                });

                listItemButton.append(buttonText);
                listItemButton.append(buttonSubText);

                listContainer.append(listItemButton);
            }

            return resolve(true);
        });
    },
    createElement({}, payload) {
        return new Promise(async (resolve) => {
            let element = document.createElement(payload.type);

            if (payload.id) {
                element.setAttribute('id', payload.id);
            }

            if (payload.class) {
                element.setAttribute('class', payload.class);
            }

            if (payload.text) {
                element.append(payload.text);
            }

            return resolve(element);
        });
    },
};

export const displayer = {
    namespaced: true,
    modules:    {},
    state,
    mutations,
    actions,
};
