const state = {};

const mutations = {};

const actions = {
    showTextLoader({ dispatch }, payload) {
        return new Promise(async (resolve) => {
            await dispatch('helpers/waitForElement', {
                target: payload.target.main,
            }, {
                root: true,
            });

            const target = document.querySelector(payload.target.main);

            if (target) {
                if (payload.target.child) {
                    await dispatch('helpers/waitForElement', {
                        target: payload.target.child,
                    }, {
                        root: true,
                    });

                    target
                        .querySelector(payload.target.child)
                        .textContent = payload.message;
                } else {
                    target
                        .textContent = payload.message;
                }
            }

            return resolve(true);
        });
    },
};

export const loader = {
    namespaced: true,
    modules:    {},
    state,
    mutations,
    actions,
};
