import Vue      from 'vue';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';

Vue.use(VueToast);

const state = {
    duration: 60000,
    position: 'bottom',
    toasts:   [],
};

const mutations = {};

const actions = {
    success({ state }, payload) {
        return new Promise(async (resolve) => {
            Vue.$toast.success(payload.message, {
                duration: payload.duration || state.duration,
                position: state.position,
            });

            return resolve(true);
        });
    },
    info({ state }, payload) {
        return new Promise(async (resolve) => {
            const instance = Vue.$toast.info(payload.message, {
                duration: payload.duration || state.duration,
                position: state.position,
            });

            state.toasts.push(instance);

            return resolve(true);
        });
    },
    warning({ state }, payload) {
        return new Promise(async (resolve) => {
            const instance = Vue.$toast.warning(payload.message, {
                duration: payload.duration || state.duration,
                position: state.position,
            });

            state.toasts.push(instance);

            return resolve(true);
        });
    },
    error({ state }, payload) {
        return new Promise(async (resolve) => {
            Vue.$toast.error(payload.message, {
                duration: payload.duration || state.duration,
                position: state.position,
            });

            return resolve(true);
        });
    },
    hide({ state }) {
        return new Promise(async (resolve) => {
            for (const toast of state.toasts) {
                setTimeout(() => {
                    toast.dismiss();
                }, 3e2);
            }

            return resolve(true);
        });
    },
};

export const toast = {
    namespaced: true,
    modules:    {},
    state,
    mutations,
    actions,
};
