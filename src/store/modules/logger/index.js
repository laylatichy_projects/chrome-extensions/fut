const state = {
    styles: {
        info:       'font-weight: bold; color: cyan;',
        success:    'font-weight: bold; color: green;',
        successAlt: 'font-weight: bold; color: forestgreen;',
        error:      'font-weight: bold; color: red;',
    },
};

const mutations = {};

const actions = {
    info({ state }, payload) {
        return new Promise(async (resolve) => {
            console.log(`%c ${payload.message}`, state.styles.info);

            return resolve(true);
        });
    },
    success({ state }, payload) {
        return new Promise(async (resolve) => {
            console.log(`%c ${payload.message}`, state.styles.success);

            return resolve(true);
        });
    },
    successAlt({ state }, payload) {
        return new Promise(async (resolve) => {
            console.log(`%c ${payload.message}`, state.styles.successAlt);

            return resolve(true);
        });
    },
    error({ state }, payload) {
        return new Promise(async (resolve) => {
            console.log(`%c ${payload.message}`, state.styles.error);

            return resolve(true);
        });
    },
};

export const logger = {
    namespaced: true,
    modules:    {},
    state,
    mutations,
    actions,
};
