const state = {};

const mutations = {};

const actions = {
    waitForElement({}, payload) {
        return new Promise(async (resolve) => {
            if (document.querySelector(payload.target)) {
                return resolve(true);
            }

            const observer = new MutationObserver(() => {
                if (document.querySelector(payload.target)) {
                    observer.disconnect();
                    return resolve(true);
                }
            });

            observer.observe(document.body, {
                childList: true,
                subtree:   true,
            });
        });
    },
    triggerMouseEvent({}, payload) {
        return new Promise(async (resolve) => {
            let event = document.createEvent('MouseEvents');

            event.initEvent(payload.type, true, true);

            payload.target.dispatchEvent(event);

            return resolve(true);
        });
    },
};

export const helpers = {
    namespaced: true,
    modules:    {},
    state,
    mutations,
    actions,
};
