import Vue from 'vue';

const state = {
    players: [],
};

const mutations = {
    setPlayer(state, payload) {
        if (payload.player?.id) {
            Vue.set(state.players, payload.player.id, payload.player);
        }
    },
};

const actions = {
    getPlayerInfo({ rootState, state, commit, dispatch }, payload) {
        return new Promise(async (resolve) => {
            const p = state.players.find((player) => player?.search_name === payload.player.name && player?.rating === parseInt(payload.player.rating));
            if (!p || p.timestamp + parseInt(process.env.VUE_APP_CACHE_TIME) < ~~(Date.now() / 1e3)) {
                await dispatch('logger/success', {
                    message: `fetching player data: ${payload.player.name}`,
                }, {
                    root: true,
                });

                chrome.runtime.sendMessage({
                    type:   'getPlayerInfo',
                    player: payload.player,
                }, async (response) => {
                    if (response?.id) {
                        chrome.runtime.sendMessage({
                            type: 'getPlayerResourceId',
                            id:   response.id,
                        }, (resourceId) => {
                            chrome.runtime.sendMessage({
                                type: 'getPlayerPrice',
                                id:   resourceId,
                            }, (data) => {
                                if (data && resourceId) {
                                    const platform = rootState.webapp.settings.platform;
                                    let player = {
                                        id:          parseInt(response.id),
                                        rating:      parseInt(response.rating),
                                        search_name: payload.player.name,
                                        full_name:   response.full_name,
                                        url_name:    response.url_name,
                                        name:        response.name,
                                        position:    response.position,
                                        futbin:      `https://www.futbin.com/22/player/${parseInt(response.id)}`,
                                        resourceId:  parseInt(resourceId),
                                        prices:      {
                                            LCPrice:  parseInt(data[resourceId].prices[platform].LCPrice.replace(/\D+/g, '')),
                                            LCPrice2: parseInt(data[resourceId].prices[platform].LCPrice2.replace(/\D+/g, '')),
                                            LCPrice3: parseInt(data[resourceId].prices[platform].LCPrice3.replace(/\D+/g, '')),
                                            // LCPrice4: parseInt(data[resourceId].prices[platform].LCPrice4.replace(/\D+/g, '')),
                                            // LCPrice5: parseInt(data[resourceId].prices[platform].LCPrice5.replace(/\D+/g, '')),
                                            updated: data[resourceId].prices[platform].updated,
                                        },
                                        timestamp:   ~~(Date.now() / 1e3),
                                    };

                                    commit('setPlayer', {
                                        player,
                                    });

                                    return resolve(player);
                                } else {
                                    return resolve(false);
                                }
                            });
                        });
                    } else {
                        let player = {
                            search_name: payload.player.name,
                            timestamp:   ~~(Date.now() / 1e3),
                        };

                        commit('setPlayer', {
                            player,
                        });

                        await dispatch('logger/error', {
                            message: `unable to fetch player data: ${payload.player.name}`,
                        }, {
                            root: true,
                        });

                        return resolve(player);
                    }
                });
            } else {
                return resolve(p);
            }
        });
    },
};

export const players = {
    namespaced: true,
    modules:    {},
    state,
    mutations,
    actions,
};
