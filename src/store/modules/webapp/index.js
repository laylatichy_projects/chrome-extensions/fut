import { club }         from './club';
import { home }         from './home';
import { leaderboards } from './leaderboards';
import { sbc }          from './sbc';
import { squad }        from './squad';
import { stadium }      from './stadium';
import { store }        from './store';
import { transfer }     from './transfer';
import { settings }     from './settings';

const state = {
    loaded: false,
    menu:   'home',
};

const mutations = {
    loaded(state) {
        state.loaded = true;
    },
    setActiveMenu(state, payload) {
        state.menu = payload.menu;
    },
};

const actions = {
    // wait for webapp main container to be loaded
    waitForWebApp({ commit }) {
        return new Promise(async (resolve) => {
            if (document.querySelector(process.env.VUE_APP_WEBAPP_MAIN_CONTAINER)) {
                return resolve(true);
            }

            const observer = new MutationObserver(() => {
                if (document.querySelector(process.env.VUE_APP_WEBAPP_MAIN_CONTAINER)) {
                    commit('loaded');
                    resolve(true);
                    observer.disconnect();
                }
            });

            observer.observe(document.body, {
                childList: true,
                subtree:   true,
            });
        });
    },
    // observe main menu change
    watchMenuChanges({ dispatch }) {
        const observer = new MutationObserver(async () => {
            await dispatch('getActiveMenu');
        });

        observer.observe(document.querySelector(process.env.VUE_APP_WEBAPP_MENU_CONTAINER), {
            childList: true,
            subtree:   true,
        });
    },
    getActiveMenu({ commit }) {
        return new Promise(async (resolve) => {
            const menuContainer = document.querySelector(process.env.VUE_APP_WEBAPP_MENU_CONTAINER);
            const active = menuContainer.querySelector('.selected')
                .classList[1]
                .split('-')
                .pop();

            commit('setActiveMenu', {
                menu: active,
            });

            return resolve(true);
        });
    },
};

export const webapp = {
    namespaced: true,
    modules:    {
        club,
        home,
        leaderboards,
        sbc,
        squad,
        stadium,
        store,
        transfer,
        settings,
    },
    state,
    mutations,
    actions,
};
