const state = {
    loaded: false,
};

const mutations = {
    loaded(state) {
        state.loaded = true;
    },
};

const actions = {
    init({}) {
        return new Promise(async (resolve) => {
            console.log('init');
            return resolve(true);
        });
    },
    destroy({}) {
        return new Promise(async (resolve) => {
            console.log('destroy');
            return resolve(true);
        });
    },
};

export const stadium = {
    namespaced: true,
    modules:    {},
    state,
    mutations,
    actions,
};
