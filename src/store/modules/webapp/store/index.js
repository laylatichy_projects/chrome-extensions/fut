const state = {
    loaded:       false,
    observers:    [],
    listeners:    [],
    subobservers: [],
    callbacks:    {
        Unassigned: 'unassigned',
    },
    targets:      {
        pageTitle: 'h1.title',
        packModal: '.ut-store-reveal-modal-list-view',
        slider:    '.slider',
        splitView: '.ut-split-view',
    },
};

const mutations = {
    loaded(state) {
        state.loaded = true;
    },
    setSubMenu(state, payload) {
        state.submenu = payload.submenu.replace(/\s/g, '');
    },
    addObserver(state, payload) {
        state.observers.push(payload.observer);
    },
    clearObservers(state) {
        state.observers.map((observer) => observer?.disconnect());
        state.observers = [];
    },
    addSubObserver(state, payload) {
        state.subobservers.push(payload.observer);
    },
    destroySubObservers(state) {
        state.subobservers.map((observer) => observer.disconnect());
    },
};

const actions = {
    init({ dispatch }) {
        return new Promise(async (resolve) => {
            await dispatch('logger/info', {
                message: 'init module:[store]',
            }, {
                root: true,
            });
            await dispatch('attachObservers');

            return resolve(true);
        });
    },
    destroy({ commit, dispatch }) {
        return new Promise(async (resolve) => {
            await dispatch('logger/info', {
                message: 'destroy module:[store]',
            }, {
                root: true,
            });
            commit('clearObservers');

            return resolve(true);
        });
    },
    attachObservers({ state, commit, dispatch }) {
        return new Promise(async (resolve) => {
            const observer = new MutationObserver(async () => {
                if (document.querySelector(state.targets.packModal)) {
                    await dispatch('getPackContent');
                }
            });

            observer.observe(document.body, {
                childList: true,
                subtree:   false,
            });

            commit('addObserver', {
                observer,
            });

            const titleObserver = new MutationObserver(async (mutation) => {
                const submenu = mutation[0]?.addedNodes[0]?.nodeValue;
                if (submenu) {
                    commit('setSubMenu', {
                        submenu,
                    });
                    await dispatch('initSubmenu');
                }
            });

            titleObserver.observe(document.querySelector(state.targets.pageTitle), {
                childList: true,
                subtree:   false,
            });

            commit('addObserver', {
                observer: titleObserver,
            });

            return resolve(true);
        });
    },
    initSubmenu({ state, commit, dispatch }) {
        return new Promise(async (resolve) => {
            commit('destroySubObservers');
            dispatch('destroyListeners');

            if (this._actions[`webapp/store/${state.callbacks[state.submenu]}`]) {
                await dispatch(state.callbacks[state.submenu]);
            }

            return resolve(true);
        });
    },
    unassigned({ state, dispatch }) {
        return new Promise(async (resolve) => {
            const observer = new MutationObserver(async () => {
                if (document.querySelector(state.targets.slider)) {
                    observer.disconnect();

                    dispatch('watchPlayerChange');
                }
            });

            observer.observe(document.querySelector(state.targets.splitView), {
                childList: true,
                subtree:   true,
            });

            return resolve(true);
        });
    },
    async watchPlayerChange({ commit, dispatch }) {
        await dispatch('toast/info', {
            message: 'calculating pack value...',
        }, {
            root: true,
        });

        await dispatch('helpers/waitForElement', {
            target: '.ut-section-header-view',
        }, {
            root: true,
        });

        setTimeout(async () => {
            let players = [];

            const itemsList = document
                .getElementsByClassName('listFUTItem');

            for (const item of itemsList) {
                const isPlayer = item.querySelector('.player');
                if (isPlayer) {
                    const player = {
                        name:   item.querySelector('.name').textContent,
                        rating: item.querySelector('.rating').textContent,
                    };
                    players.push(player);
                }
            }

            const uniquePlayers = players
                .filter((v, i, a) => a.findIndex((t) => (
                    JSON.stringify(t) === JSON.stringify(v))) === i,
                );

            let [...response] = await Promise.all(uniquePlayers.map((player) => dispatch('players/getPlayerInfo', {
                player,
            }, {
                root: true,
            })));

            let data = {
                playersCount:   0,
                playersExtinct: 0,
                packValue:      0,
            };

            for (const player of response) {
                data.playersCount++;
                if (player.prices) {
                    data.packValue += player.prices.LCPrice;
                }
                if (player.prices?.LCPrice === 0) {
                    data.playersExtinct++;
                }
            }

            data.packValue *= 0.95;

            await dispatch('toast/hide', {}, {
                root: true,
            });

            await dispatch('loader/showTextLoader', {
                message: `${data.playersCount} players, extinct: ${data.playersExtinct}, value: ${data.packValue}`,
                target:  {
                    main:  '.ut-section-header-view',
                    child: 'h2.title',
                },
            }, {
                root: true,
            });

            const observer = new MutationObserver(async () => {
                await dispatch('getCurrentPlayer');
            });

            observer.observe(document.querySelector('.slider'), {
                childList:  true,
                attributes: true,
            });

            commit('addSubObserver', {
                observer,
            });

            await dispatch('getCurrentPlayer');

            const itemLists = document.querySelectorAll('.listFUTItem');

            const actionsObserver = new MutationObserver(async () => {
                observer.disconnect();
                actionsObserver.disconnect();
                dispatch('watchPlayerChange');
            });

            for (const itemList of itemLists) {
                actionsObserver.observe(itemList, {
                    childList: true,
                    subtree:   false,
                });
            }

            commit('addSubObserver', {
                observer: actionsObserver,
            });
        }, 1e2);
    },
    getCurrentPlayer({ dispatch }) {
        return new Promise(async (resolve) => {
            await dispatch('helpers/waitForElement', {
                // target: '.tns-slide-active > .player',
                target: '.tns-slide-active',
            }, {
                root: true,
            });

            const playerContainer = document
                .querySelector('.tns-slide-active > .player');

            if (playerContainer) {
                const player = {
                    name:   playerContainer.querySelector('.name').textContent,
                    rating: playerContainer.querySelector('.rating').textContent,
                };

                const response = await dispatch('players/getPlayerInfo', {
                    player,
                }, {
                    root: true,
                });

                await dispatch('displayer/displayPlayerInfo', {
                    player: response,
                }, {
                    root: true,
                });
            }

            return resolve(true);
        });
    },
    getPackContent({ dispatch }) {
        return new Promise(async (resolve) => {
            await dispatch('toast/info', {
                message: 'calculating pack value...',
            }, {
                root: true,
            });

            let players = [];

            const itemsList = document
                .getElementsByClassName('listFUTItem');

            for (const item of itemsList) {
                const isPlayer = item.querySelector('.player');
                if (isPlayer) {
                    const player = {
                        name:   item.querySelector('.name').textContent,
                        rating: item.querySelector('.rating').textContent,
                    };
                    players.push(player);
                }
            }

            const uniquePlayers = players
                .filter((v, i, a) => a.findIndex((t) => (
                    JSON.stringify(t) === JSON.stringify(v))) === i,
                );

            let [...response] = await Promise.all(uniquePlayers.map((player) => dispatch('players/getPlayerInfo', {
                player,
            }, {
                root: true,
            })));

            let data = {
                playersCount:   0,
                playersExtinct: 0,
                packValue:      0,
                packProfit:     0,
            };

            let packPrice = parseInt(document
                .querySelector('.ut-store-reveal-modal-list-view')
                .querySelector('.currency.coins > .text')
                .textContent
                .replace(/,/g, ''));

            for (const player of response) {
                data.playersCount++;
                if (player.prices) {
                    data.packValue += player.prices.LCPrice;
                }
                if (player.prices?.LCPrice === 0) {
                    data.playersExtinct++;
                }
            }

            data.packValue *= 0.95;
            data.packProfit = data.packValue - packPrice;

            await dispatch('toast/hide', {}, {
                root: true,
            });

            await dispatch('loader/showTextLoader', {
                message: `${data.playersCount} players, extinct: ${data.playersExtinct}, value: ${data.packValue}, profit: ${data.packProfit}`,
                target:  {
                    main:  '.ut-store-reveal-modal-list-view',
                    child: 'h1',
                },
            }, {
                root: true,
            });

            return resolve(true);
        });
    },
    addListener({ state, dispatch }, payload) {
        state.listeners.push({
            element:  payload.element,
            type:     payload.type,
            callback: payload.callback,
        });

        payload.element.addEventListener(payload.type, () => {
            dispatch(payload.callback);
        });
    },
    destroyListeners({ state, dispatch }) {
        state.listeners.map((listener) => listener.element.removeEventListener(listener.type, () => {
            dispatch(listener.callback);
        }));
    },
};

export const store = {
    namespaced: true,
    modules:    {},
    state,
    mutations,
    actions,
};
