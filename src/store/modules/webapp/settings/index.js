const state = {
    loaded:   false,
    platform: 'ps', // set platform to playstation by default
};

const mutations = {
    loaded(state) {
        state.loaded = true;
    },
    setPlatform(state, payload) {
        state.platform = payload.platform;
    },
};

const actions = {
    init({ dispatch }) {
        return new Promise(async (resolve) => {
            await dispatch('logger/info', {
                message: 'init module:[settings]',
            }, {
                root: true,
            });
            await dispatch('getPlatform');

            return resolve(true);
        });
    },
    destroy({ dispatch }) {
        return new Promise(async (resolve) => {
            await dispatch('logger/info', {
                message: 'destroy module:[settings]',
            }, {
                root: true,
            });

            return resolve(true);
        });
    },
    getPlatform({ commit, dispatch }) {
        return new Promise(async (resolve) => {
            let platform;
            // TODO: other platforms mapping
            switch (document.getElementsByClassName('platform')[0].classList[1]) {
                case 'playstation':
                    await dispatch('toast/info', {
                        message: 'platform set to: playstation',
                        duration: 5000,
                    }, {
                        root: true,
                    });

                    platform = 'ps';

                    break;
                default:
                    await dispatch('toast/error', {
                        message: 'unrecognised platform',
                        duration: 5000,
                    }, {
                        root: true,
                    });

                    console.log('todo other platforms');
            }

            commit('setPlatform', {
                platform,
            });

            return resolve(true);
        });
    },
};

export const settings = {
    namespaced: true,
    modules:    {},
    state,
    mutations,
    actions,
};
