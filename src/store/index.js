import { mutations }        from '@/store/mutations';
import { state }            from '@/store/state';
import { actions }          from '@/store/actions';
import { webapp }           from '@/store/modules/webapp';
import { players }          from '@/store/modules/players';
import { loader }           from '@/store/modules/loader';
import { helpers }          from '@/store/modules/helpers';
import { displayer }        from '@/store/modules/displayer';
import { logger }           from '@/store/modules/logger';
import { toast }            from '@/store/modules/toast';
import Vue                  from 'vue';
import Vuex                 from 'vuex';
import createPersistedState from 'vuex-persistedstate';


const playersCache = createPersistedState({
    key:   'laylatichy-futbin-players',
    paths: ['players'],
});


const webappSettingsCache = createPersistedState({
    key:   'laylatichy-futbin-settings',
    paths: ['webapp.settings'],
});

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        webapp,
        players,
        loader,
        helpers,
        displayer,
        logger,
        toast,
    },
    state,
    actions,
    mutations,
    plugins: [
        playersCache,
        webappSettingsCache,
    ],
});

export default store;
